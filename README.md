# AWS Federated Container Tabs

## What
A Firefox add-on that augments the AWS Identity Center's single-sign-on Portal, by opening each unique accountId+role combination in its own contextual identity tab.

## Development
### testing locally
- `npm run start:firefox` loads the plugin into a sandbox profile in Firefox developer edition. Depends upon `SSO_START_URL` being set in the shell environment.

### signing for distribution
- commit the locally-tested changes
- update the manifest to a new version
- build the distribution package: `npm run build`
- establish the `AMO_JWT_ISSUER` and `AMO_JWT_SECRET` environment variables using values obtained from https://addons.mozilla.org/en-US/developers/addon/api/key/.
- `npm run sign` and await validation and download of .xpi file.
- commit the manifest change & push
- use `about:addons` to load the add-on from the .xpi file.

## License
- Icon from: https://www.iconfinder.com/icons/290119/card_id_identification_identity_profile_icon#size=128, License: "Free for commercial use".
- Add-on code: (BSD-3-Clause)
    ```
    Copyright 2024 Tommy Knowlton
    
    Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
    
    1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    
    2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
    
    3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
    
    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
    ```
