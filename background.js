(async () => {
    const {contextualIdentities, tabs, webRequest} = browser;

    async function stableChooseIconAndColor(name) {
        const COLORS = ['blue', 'turquoise', 'green', 'yellow', 'orange', 'red', 'pink', 'purple', 'toolbar'];
        const CI_ICON = 'fingerprint';

        const encoder = new TextEncoder();
        data = encoder.encode(name);
        let hash = new Uint8Array(await crypto.subtle.digest('SHA-1', data));
        let lastByte = hash.at(-1);
        let color = COLORS[lastByte % COLORS.length];

        return [color, CI_ICON];
    }

    async function getOrCreateCI(location) {
        let url = new URL(location);
        url = new URL(url.searchParams.get('Issuer'));
        // Issuer is a URL whose hash includes a suffix we extract in order to parse it as a URL query
        let query = url.hash.substring(url.hash.indexOf('?'));
        url = new URL('https://example.com' + query);

        let awsAccount = url.searchParams.get('account_id');
        let roleName = url.searchParams.get('role_name');
        let name = `${awsAccount}//${roleName}`;

        return contextualIdentities.query({name})
            .then(async cis => {
                switch (cis.length) {
                    case 0:
                        // none found, create one and return it.
                        [color, icon] = await stableChooseIconAndColor(name);

                        return contextualIdentities.create({
                            name, color, icon
                        });
                    case 1:
                        // exactly one found, return that one.
                        return cis[0];
                    default:
                        throw "Found more than one container with name ${name}, refusing to choose."
                }
            });
    }

    async function replaceTab(requestDetails) {
        if (requestDetails.cookieStoreId !== 'firefox-default') {
            return;
        }

        return getOrCreateCI(requestDetails.url)
            .then((contextualIdentity) => {
                return tabs.get(requestDetails.tabId)
                    .then((oldTab) => Promise.all([
                        // use oldTab details to remove oldTab and create newTab at same index where oldtab was
                        tabs.remove([oldTab.id]),
                        tabs.create({
                            cookieStoreId: contextualIdentity.cookieStoreId,
                            url: requestDetails.url,
                            index: oldTab.index
                        })
                    ]));
            }).finally(() => ({cancel: true}));
    }
    webRequest.onBeforeRequest.addListener(replaceTab, {urls: ['https://signin.aws.amazon.com/*']}, ['blocking']);
})().then(() => {
    const ADD_ON_NAME = 'ff-aws-federated-tabs';
    console.log(`Loaded ${ADD_ON_NAME} at ${new Date()}`);
});
